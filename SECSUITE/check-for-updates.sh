#!/bin/bash
#
#
# This addition to Secsuite (check-for-updates script) has been written with love, anger & hatred.
#
# This script is intended to proceed with the following actions for the following types of new code:
#
# Standard New Release: This script will notify users that there is a new release available, unfortunately forcing
#                       users to manually import the code into the designated directory. Information will be provided
#                       to the users, in the form of a text file, located at: /root/scripts/SECSUITE/update-available.txt
#
# Security Patch        This script will both notify users of a forced update on one, or multiple probe(s),
#                       due to an unfortunate security vulnerability in Secsuite's code.
#                       This script will not transmit (TX) any data to secsuite.net, nor will it communicate,
#                       by any method, nor for any reason, any information regarding your system's account information.
#                       In addittion to performing automatic security patches, Secsuite will always log its actions,
#                       in the form of a text file, located at: /root/scripts/SECSUITE/security-patch-notif.txt
#
# Standard Releases are scheduled (roughly) once every two months. These are optional, however may include new probes.
# Security patches may be published at any time, without warning, therefore, it is suggested to run it in your cronjobs
# to check every day (at 23:30 (or whichever time is most convenient), as not to impose on your business operations).
#
# For more information regarding this process, please contact dan@danward.info or dw@securitechsystems.ca
#
#--------------------------------------------------------------
#
#
#Directory Variables:
#Package Directories
basedir="/root/scripts/SECSUITE"
basedirformat="\/root\/scripts\/SECSUITE"
inframondir="$basedir/inframon"
#
#
#SECURITY Patches
confirmpackages="$legacydir/confirmpackages.txt"
iliketomoveit="$basedir/mover.sh"
legacydir="$basedir/updateservice"
legacydirformat="$basedirformat\/updateservice\/"
legacyfiles="$legacydir/contents.txt"
#
#
#Define current version install of SECSUITE:
currentlyinstalled="v3.080820A"
#
which curl >> $basedir/curlcheck.txt
if grep -q "curl" "$basedir/curlcheck.txt"; then
  echo "curl is installed, continuing..."
  rm -rf $basedir/curlcheck.txt
else
  echo "+ Installing curl..."
  apt-get install curl -y &>/dev/null
  rm -rf $basedir/curlcheck.txt
fi

#
#Get Latest Version of SECSUITE from source:
curl https://secsuite.net/currentversion/ 2>/dev/null >> $basedir/latestrelease.txt
updatever=$(cat $basedir/latestrelease.txt | grep -E "v")






#Determine whether version has been patched for security, or just normal release:

# No Available Updates;
if [ -f $basedir/latestrelease.txt ]; then
  echo "$basedir/latestrelease.txt Exists"
        if grep -q "noupdates" "$basedir/latestrelease.txt"; then
            echo "$(date) - Your current version of SECSUITE ($updatever) is the latest version. There are no updates or patches." >> $basedir/no-updates-due.txt
            cat $basedir/no-updates-due.txt
        fi
fi





# Issued Regular Release;
if [ -f $basedir/latestrelease.txt ]; then
        if grep -q "release" "$basedir/latestrelease.txt"; then
            echo "$(date) - There is a new version of SECSUITE available for download. Please see https://secsuite.net for more information." >> $basedir/update-available.txt
        fi
fi





# Issued Security Patch
if grep -q "patch" "$basedir/latestrelease.txt"; then
   echo "$(date) - There is a new security patch for SECSUITE available. Downloading from secsuite.net..." >> $basedir/security-patch-notif.txt
   wget https://secsuite.net/currentversion/patch.tar.gz 2>/dev/null
   tar -zxvf $basedir/patch.tar.gz &>/dev/null ; rm $basedir/patch.tar.gz
   echo ''
#
   if ! [ -f $legacydir ]; then
      echo "$legacydir does not exist. Creating."
      mkdir $legacydir
   fi
#
#
   echo "The Affected Probes will now be updated: " >> $basedir/security-patch-notif.txt
   cat $basedir/package_list.txt >> $basedir/security-patch-notif.txt
#
#
   input="$basedir/package_list.txt"
   #Begin constructing script to move probes into legacy directory;
   echo "#!/bin/bash" >> $iliketomoveit
   while IFS= read -r line
   do
     echo "mv $line* $(echo $legacydir/)" >> $iliketomoveit &>/dev/null
   done < "$input"
   echo ''
   mkdir $legacydir/
   bash $iliketomoveit
   ls $legacydir/*.sh* >> $legacyfiles &>/dev/null
   echo "Legacy File List:" >> $basedir/security-patch-notif.txt
   cat $legacyfiles >> $basedir/security-patch-notif.txt
   input="$legacyfiles"
   #Begin identifying probes:
   sed -i "s/$legacydirformat//g" $legacyfiles
   while IFS= read -r line
   do
     echo "$line" >> $confirmpackages
   done < "$input"
   echo ''
   echo "Confirmed Packages in Legacy Directory:" >> $basedir/security-patch-notif.txt
   cat $legacyfiles >> $basedir/security-patch-notif.txt
   echo ''
   while IFS= read -r line
   do
   if grep -q "$line" "$confirmpackages"; then
       echo "Package $line is confirmed for patching. Continuing..." >> $basedir/security-patch-notif.txt
   fi
   done < "$input"





   if grep -q "apache" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #-------------------------------------
   #Apache:
   mv $inframondir/apachestatus/apache-monitor*.sh $legacydir/
   cd $legacydir/
   ls -ls apache-monitor-*.sh | awk '{print $10}' >> $basedir/apachefilesdirlist.txt
   sed -i 's/apache\-monitor\.sh//g' $basedir/apachefilesdirlist.txt
   cd $basedir/
   while IFS= read -r line; do
   apachemysqlusr=$(cat $legacydir/$line | grep -E "mysqluser=" > $basedir/apachemysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/apachemysqlusr.txt ; sed -i "s/'//g" $basedir/apachemysqlusr.txt ; cat $basedir/apachemysqlusr.txt 2>/dev/null)
   apachemysqlpass=$(cat $legacydir/$line | grep -E "mysqlpass=" > $basedir/apachemysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/apachemysqlpass.txt ; sed -i "s/'//g" $basedir/apachemysqlpass.txt ; cat $basedir/apachemysqlpass.txt 2>/dev/null)
   echoapacheusr=$(cat $basedir\/apachemysqlusr.txt)
   echoapachepass=$(cat $basedir\/apachemysqlpass.txt)
   monitorname=$(echo $line)
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   mv $basedir/apache-monitor.sh $basedir/inframon/apachestatus/$line
   sed -i "s/mysqluser='user'/mysqluser='$echoapacheusr'/g" $basedir/inframon/apachestatus/$line
   sed -i "s/mysqlpass='pass'/mysqlpass='$echoapachepass'/g" $basedir/inframon/apachestatus/$line
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/apachestatus/$line ; bash $inframondir/apachestatus/$line
   echo ''
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echoapacheusr" --password="$echoapachepass" -e "USE status; SELECT * FROM hist_apachestatus order by timestamp desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   rm -rf $basedir/apachemysqlusr.txt $basedir/apachemysqlpass.txt $basedir/apachefilesdirlist.txt
   done < $basedir/apachefilesdirlist.txt
   fi




   if grep -q "bandwidth" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #Bandwidth
   echo "Bandwidth Monitor has been issued a security patch." >> $basedir/security-patch-notif.txt
   #bandwidth:
   mv $basedir/inframon/bandwidth/bandwidth* $legacydir/
   bandwidthmysqlusr=$(cat $legacydir/bandwidth-monitor* | grep -E "mysqluser=" > $basedir/bandwidthmysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/bandwidthmysqlusr.txt ; sed -i "s/'//g" $basedir/bandwidthmysqlusr.txt ; cat $basedir/bandwidthmysqlusr.txt 2>/dev/null)
   bandwidthmysqlpass=$(cat $legacydir/bandwidth-monitor* | grep -E "mysqlpass=" > $basedir/bandwidthmysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/bandwidthmysqlpass.txt ; sed -i "s/'//g" $basedir/bandwidthmysqlpass.txt ; cat $basedir/bandwidthmysqlpass.txt 2>/dev/null)
   bandwidthhostname=$(cat $legacydir/bandwidth-monitor* | grep -E "hostname=" > $basedir/bandwidthhostname.txt ; sed -i 's/hostname=//g' $basedir/bandwidthhostname.txt ; sed -i "s/'//g" $basedir/bandwidthhostname.txt ; cat $basedir/bandwidthhostname.txt 2>/dev/null)
   bandwidthinterface=$(cat $legacydir/bandwidth-monitor* | grep -E "interface=" > $basedir/bandwidthinterface.txt ; sed -i 's/interface=//g' $basedir/bandwidthinterface.txt ; sed -i "s/'//g" $basedir/bandwidthinterface.txt ; cat $basedir/bandwidthinterface.txt 2>/dev/null)
   echobandwidthusr=$(cat $basedir\/bandwidthmysqlusr.txt)
   echobandwidthpass=$(cat $basedir\/bandwidthmysqlpass.txt)
   echobandwidthhostname=$(cat $basedir\/bandwidthhostname.txt)
   echobandwidthinterface=$(cat $basedir\/bandwidthinterface.txt)
   monitorname=$(cat $confirmpackages)
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/bandwidth-monitor.sh $basedir/$monitorname
   sed -i "s/mysqluser='user'/mysqluser='$echobandwidthusr'/g" $basedir/$monitorname
   sed -i "s/mysqlpass='pass'/mysqlpass='$echobandwidthpass'/g" $basedir/$monitorname
   sed -i "s/hostname='hostname'/hostname='$echobandwidthhostname'/g" $basedir/$monitorname
   sed -i "s/interface='interface'/interface='$echobandwidthinterface'/g" $basedir/$monitorname
   echo ''
   echo "Displaying MySQL User of new monitor: $(cat $basedir/$monitorname | grep -E 'mysqluser=')" >> $basedir/security-patch-notif.txt
   echo "Moving monitor to its normal directory..." >> $basedir/security-patch-notif.txt
   mv $basedir/$monitorname $inframondir/bandwidth/
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/bandwidth/$monitorname ; bash $inframondir/bandwidth/$monitorname
   echo ''
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echobandwidthusr" --password="$echobandwidthpass" -e "USE status; SELECT * FROM bandwidthstatus;" 2>/dev/null >> $basedir/security-patch-notif.txt
   rm -rf $basedir/bandwidthmysqlusr.txt $basedir/bandwidthmysqlpass.txt $basedir/bandwidthhostname.txt $basedir/bandwidthinterface.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   fi





   if grep -q "cpu-load" "$basedir/package_list.txt"; then
   echo "CPU Load Average Monitor has been issued a patch." >> $basedir/security-patch-notif.txt

   #CPU Load Average Monitor:
   cp $basedir/inframon/cpufiles/*/cpu-load-monitor.sh $legacydir/
   ls -la $legacydir/
   ls -lsrhd $basedir/inframon/cpufiles/* | awk '{print $10}' >> $basedir/cpufilesdirlist.txt
   echo "Directories found:"
   cat $basedir/cpufilesdirlist.txt
   while IFS= read -r line; do
   #
   cpuloadmysqlusr=$(cat $legacydir/cpu-load-monitor.sh | grep -E "mysqluser=" > $basedir/cpuloadmysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/cpuloadmysqlusr.txt ; sed -i "s/'//g" $basedir/cpuloadmysqlusr.txt ; cat $basedir/cpuloadmysqlusr.txt 2>/dev/null)
   cpuloadmysqlpass=$(cat $legacydir/cpu-load-monitor.sh | grep -E "mysqlpass=" > $basedir/cpuloadmysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/cpuloadmysqlpass.txt ; sed -i "s/'//g" $basedir/cpuloadmysqlpass.txt ; cat $basedir/cpuloadmysqlpass.txt 2>/dev/null)
   cpuloadhostname=$(cat $legacydir/cpu-load-monitor.sh | grep -E "hostname=" > $basedir/cpuloadhostname.txt ; sed -i 's/hostname=//g' $basedir/cpuloadhostname.txt ; sed -i "s/'//g" $basedir/cpuloadhostname.txt ; cat $basedir/cpuloadhostname.txt 2>/dev/null)
   echocpuloadusr=$(cat $basedir\/cpuloadmysqlusr.txt)
   echocpuloadpass=$(cat $basedir\/cpuloadmysqlpass.txt)
   echocpuloadhostname=$(cat $basedir\/cpuloadhostname.txt)
   #
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/cpu-load-monitor.sh $line/cpu-load-monitor.sh
   #
   sed -i "s/mysqluser='user'/mysqluser='$echocpuloadusr'/g" $line/cpu-load-monitor.sh
   sed -i "s/mysqlpass='pass'/mysqlpass='$echocpuloadpass'/g" $line/cpu-load-monitor.sh
   sed -i "s/hostname='hostname'/hostname='$echocpuloadhostname'/g" $line/cpu-load-monitor.sh
   echo ''
   #
   echo "Displaying MySQL User of new monitor: $(cat $line/cpu-load-monitor.sh | grep -E 'mysqluser=')" >> $basedir/security-patch-notif.txt
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $line/cpu-load-monitor.sh ; bash $line/load-avg*
   echo ''
   chmod +x $line/cpu-load-monitor.sh
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echocpuloadusr" --password="$echocpuloadpass" -e "USE status; SELECT * FROM hist_cpu order by id desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   #
   rm -rf $basedir/cpuloadmysqlusr.txt $basedir/cpuloadmysqlpass.txt $basedir/cpuloadhostname.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   done < $basedir/cpufilesdirlist.txt
   rm -rf $basedir/cpufilesdirlist.txt
   fi





   if grep -q "diskusage" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #Disk Usage
   echo "Disk Monitor has been issued a security patch." >> $basedir/security-patch-notif.txt
   #bandwidth:
   mv $basedir/inframon/diskusage/disk-monitor-local-probe.sh $legacydir/
   ls -la $legacydir/
   diskmysqlusr=$(cat $legacydir/disk-monitor-local-probe.sh | grep -E "mysqluser=" > $basedir/diskmysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/diskmysqlusr.txt ; sed -i "s/'//g" $basedir/diskmysqlusr.txt ; cat $basedir/diskmysqlusr.txt 2>/dev/null)
   diskmysqlpass=$(cat $legacydir/disk-monitor-local-probe.sh | grep -E "mysqlpass=" > $basedir/diskmysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/diskmysqlpass.txt ; sed -i "s/'//g" $basedir/diskmysqlpass.txt ; cat $basedir/diskmysqlpass.txt 2>/dev/null)
   diskhostname=$(cat $legacydir/disk-monitor-local-probe.sh | grep -E "hostname=" > $basedir/diskhostname.txt ; sed -i 's/hostname=//g' $basedir/diskhostname.txt ; sed -i "s/'//g" $basedir/diskhostname.txt ; cat $basedir/diskhostname.txt 2>/dev/null)
   echodiskusr=$(cat $basedir\/diskmysqlusr.txt)
   echodiskpass=$(cat $basedir\/diskmysqlpass.txt)
   echodiskhostname=$(cat $basedir\/diskhostname.txt)
   monitorname=$(cat $confirmpackages)
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/disk-monitor-local-probe.sh $basedir/inframon/diskusage/
   sed -i "s/mysqluser='username'/mysqluser='$echodiskusr'/g" $basedir/inframon/diskusage/disk-monitor-local-probe.sh
   sed -i "s/mysqlpass='password'/mysqlpass='$echodiskpass'/g" $basedir/inframon/diskusage/disk-monitor-local-probe.sh
   sed -i "s/hostname='hostname'/hostname='$echodiskhostname'/g" $basedir/inframon/diskusage/disk-monitor-local-probe.sh
   echo ''
   echo "Displaying MySQL User of new monitor: $(cat $basedir/inframon/diskusage/disk-monitor-local-probe.sh | grep -E 'mysqluser=')" >> $basedir/security-patch-notif.txt
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/diskusage/disk-monitor-local-probe.sh ; bash $inframondir/diskusage/disk-monitor-local-probe.sh
   echo ''
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echodiskusr" --password="$echodiskpass" -e "USE status; SELECT * FROM hist_storagestatus order by id desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   rm -rf $basedir/diskmysqlusr.txt $basedir/diskmysqlpass.txt $basedir/diskhostname.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   fi





   if grep -q "latency-monitor" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #Latency Monitor
   echo "Latency Monitor has been issued a security patch." >> $basedir/security-patch-notif.txt
   #Latency:
   mv $basedir/inframon/latency-files/*-latency-monitor.sh $legacydir/
   cd $legacydir/
   ls -ls *.sh* | awk '{print $10}' >> $basedir/latencydirlist.txt
   cd $basedir/
   echo "Probes which require security patches:" >> $basedir/security-patch-notif.txt
   cat $basedir/latencydirlist.txt >> $basedir/security-patch-notif.txt
#  Please forgive me, I have less than 26h until Defcon. I need this done now.
   while IFS= read -r line; do
   latencymysqlusr=$(cat $legacydir/$line | grep -A2 "Global Variables" | grep -E "user" > $basedir/latencymysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/latencymysqlusr.txt ; sed -i "s/'//g" $basedir/latencymysqlusr.txt ; sed -i "s/user\=//g" $basedir/latencymysqlusr.txt ; cat $basedir/latencymysqlusr.txt 2>/dev/null)
   latencymysqlpass=$(cat $legacydir/$line | grep -E "pass=" > $basedir/latencymysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/latencymysqlpass.txt ; sed -i "s/'//g" $basedir/latencymysqlpass.txt ; sed -i 's/pass\=//g' $basedir/latencymysqlpass.txt ; cat $basedir/latencymysqlpass.txt 2>/dev/null)
   latencyhostname=$(cat $legacydir/$line | grep -E "hostname=" > $basedir/latencyhostname.txt ; sed -i 's/hostname=//g' $basedir/latencyhostname.txt ; sed -i "s/'//g" $basedir/latencyhostname.txt ; cat $basedir/latencyhostname.txt 2>/dev/null)
   sed -i 's/user\=//g' $basedir/latencymysqlusr.txt
   echolatencyusr=$(cat $basedir\/latencymysqlusr.txt)
   sed -i 's/pass\=//g' $basedir/latencymysqlpass.txt
   echolatencypass=$(cat $basedir\/latencymysqlpass.txt)
   echolatencyhostname=$(cat $basedir\/latencyhostname.txt)
   monitorname=$(echo $line)
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/latency-monitor.sh $basedir/inframon/latency-files/$line
   sed -i "s/user='username'/user='$echolatencyusr'/g" $basedir/inframon/latency-files/$line
   sed -i "s/pass='password'/pass='$echolatencypass'/g" $basedir/inframon/latency-files/$line
   sed -i "s/hostname='hostname'/hostname='$echolatencyhostname'/g" $basedir/inframon/latency-files/$line
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/latency-files/$line ; bash $inframondir/latency-files/$line
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echolatencyusr" --password="$echolatencypass" -e "USE status; SELECT * FROM hist_srv order by id desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   rm -rf $basedir/latencymysqlusr.txt $basedir/latencymysqlpass.txt $basedir/latencyhostname.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   done < $basedir/latencydirlist.txt
   rm -rf $basedir/latencydirlist.txt
   fi





   if grep -q "memory" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #Memory Usage
   echo "Memory (RAM) Monitor has been issued a security patch." >> $basedir/security-patch-notif.txt
   #bandwidth:
   mv $basedir/inframon/memory/memory-monitor-local-probe.sh $legacydir/
   ls -la $legacydir/
   memorymysqlusr=$(cat $legacydir/memory-monitor-local-probe.sh | grep -E "mysqluser=" > $basedir/memorymysqlusr.txt ; sed -i 's/mysqluser=//g' $basedir/memorymysqlusr.txt ; sed -i "s/'//g" $basedir/memorymysqlusr.txt ; cat $basedir/memorymysqlusr.txt 2>/dev/null)
   memorymysqlpass=$(cat $legacydir/memory-monitor-local-probe.sh | grep -E "mysqlpass=" > $basedir/memorymysqlpass.txt ; sed -i 's/mysqlpass=//g' $basedir/memorymysqlpass.txt ; sed -i "s/'//g" $basedir/memorymysqlpass.txt ; cat $basedir/memorymysqlpass.txt 2>/dev/null)
   memoryhostname=$(cat $legacydir/memory-monitor-local-probe.sh | grep -E "hostname=" > $basedir/memoryhostname.txt ; sed -i 's/hostname=//g' $basedir/memoryhostname.txt ; sed -i "s/'//g" $basedir/memoryhostname.txt ; cat $basedir/memoryhostname.txt 2>/dev/null)
   echomemoryusr=$(cat $basedir\/memorymysqlusr.txt)
   echomemorypass=$(cat $basedir\/memorymysqlpass.txt)
   echomemoryhostname=$(cat $basedir\/memoryhostname.txt)
   monitorname=$(cat $confirmpackages)
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/memory-monitor-local-probe.sh $basedir/inframon/memory/
   sed -i "s/mysqluser='username'/mysqluser='$echomemoryusr'/g" $basedir/inframon/memory/memory-monitor-local-probe.sh
   sed -i "s/mysqlpass='password'/mysqlpass='$echomemorypass'/g" $basedir/inframon/memory/memory-monitor-local-probe.sh
   sed -i "s/hostname='hostname'/hostname='$echomemoryhostname'/g" $basedir/inframon/memory/memory-monitor-local-probe.sh
   echo ''
   echo "Displaying MySQL User of new monitor: $(cat $basedir/inframon/memory/memory-monitor-local-probe.sh | grep -E 'mysqluser=')" >> $basedir/security-patch-notif.txt
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/memory/memory-monitor-local-probe.sh ; bash $inframondir/memory/memory-monitor-local-probe.sh
   echo ''
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echomemoryusr" --password="$echomemorypass" -e "USE status; SELECT * FROM hist_memorystatus order by id desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   rm -rf $basedir/memorymysqlusr.txt $basedir/memorymysqlpass.txt $basedir/memoryhostname.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   fi




   if grep -q "temperatures" "$basedir/package_list.txt"; then
      echo "CPU Temperature monitor has been issued a security patch. This probe is regarded as the least used." >> $basedir/security-patch-notif.txt
      echo "Please refer to github.com/ghostinthecable/secsuite-production/ for the latest (patched) version." >> $basedir/security-patch-notif.txt
   fi




   if grep -q "users" "$basedir/package_list.txt"; then
   #Get Variables from previous package:
   #Logged Users
   echo "Users Monitor has been issued a security patch." >> $basedir/security-patch-notif.txt
   #bandwidth:
   mv $basedir/inframon/users/users-monitor-local.sh $legacydir/
   ls -la $legacydir/
   usersmysqlusr=$(cat $legacydir/users-monitor-local.sh | grep -E "user='" > $basedir/usersmysqlusr.txt ; sed -i 's/user=//g' $basedir/usersmysqlusr.txt ; sed -i "s/'//g" $basedir/usersmysqlusr.txt ; cat $basedir/usersmysqlusr.txt 2>/dev/null)
   usersmysqlpass=$(cat $legacydir/users-monitor-local.sh | grep -E "pass='" > $basedir/usersmysqlpass.txt ; sed -i 's/pass=//g' $basedir/usersmysqlpass.txt ; sed -i "s/'//g" $basedir/usersmysqlpass.txt ; cat $basedir/usersmysqlpass.txt 2>/dev/null)
   echousersusr=$(cat $basedir\/usersmysqlusr.txt)
   echouserspass=$(cat $basedir\/usersmysqlpass.txt)
   monitorname=$(cat $confirmpackages)
   wget https://secsuite.net/currentversion/packages.tar.gz 2>/dev/null
   tar -zxvf $basedir/packages.tar.gz &>/dev/null ; rm $basedir/packages.tar.gz
   echo "Name to give monitor: $monitorname" >> $basedir/security-patch-notif.txt
   mv $basedir/users-monitor-local.sh $basedir/inframon/users/
   sed -i "s/user='username'/user='$echousersusr'/g" $basedir/inframon/users/users-monitor-local.sh
   sed -i "s/pass='password'/pass='$echouserspass'/g" $basedir/inframon/users/users-monitor-local.sh
   echo "Displaying MySQL User of new monitor: $(cat $basedir/inframon/users/users-monitor-local.sh | grep -E 'mysqluser=')" >> $basedir/security-patch-notif.txt
   echo "Applying Permissions & executing..." >> $basedir/security-patch-notif.txt ; chmod 755 $inframondir/users/users-monitor-local.sh ; bash $inframondir/users/users-monitor-local.sh
   echo "Reading data with credentials from previous probe..." >> $basedir/security-patch-notif.txt
   mysql --user="$echousersusr" --password="$echouserspass" -e "USE status; SELECT * FROM hist_loggedusers order by backuptime desc limit 1;" 2>/dev/null >> $basedir/security-patch-notif.txt
   rm -rf $basedir/usersmysqlusr.txt $basedir/usersmysqlpass.txt $basedir/usershostname.txt
   echo "Your probe has been matched, and moved back into it's designated directory." >> $basedir/security-patch-notif.txt
   fi

# End of process, clear all temporary files;
  rm -rf $basedir/package_list.txt $iliketomoveit $legacydir $legacyfiles $confirmpackages
fi

# End update process;
rm $basedir/latestrelease.txt

# Adios
exit
