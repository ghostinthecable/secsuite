#!/bin/bash
#
basedir="/root/scripts/SECSUITE/BroadEye"
#
#Apache;
bash $basedir/apache-intel.sh
#Bandwidth;
bash $basedir/bandwidth-intel.sh
#CPU Load;
bash $basedir/cpu-load-intel.sh
#Disk Usage;
bash $basedir/disk-intel.sh
#Latency Report;
bash $basedir/latency-intel.sh
#Memory Report;
bash $basedir/memory-intel.sh
#
exit
