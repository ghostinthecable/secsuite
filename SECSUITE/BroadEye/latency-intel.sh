#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
minlatencies="$basedir/minlatency.txt"
maxlatencies="$basedir/maxlatency.txt"
yd="$basedir/yesdata.txt"
#
#SQL Queries;
quickestping=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT hostname AS '', min(lastping) AS '' FROM hist_srv WHERE importtime BETWEEN '$range24h' AND '$timenow' GROUP BY hostname;" 2>/dev/null >> $minlatencies ; sed -i 's/Host is up (//g' $minlatencies ; sed -i 's/latency)\.//g' $minlatencies ; column -t -s $'\t' $minlatencies ; cat $minlatencies > $yd ; rm $minlatencies)
longestping=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT hostname AS '', max(lastping) AS '' FROM hist_srv WHERE importtime BETWEEN '$range24h' AND '$timenow' GROUP BY hostname;" 2>/dev/null >> $maxlatencies ; sed -i 's/Host is up (//g' $maxlatencies ; sed -i 's/latency)\.//g' $maxlatencies ; column -t -s $'\t' $maxlatencies ; rm $maxlatencies)

#---------------------

ydlc=$(cat $yd | wc -l)

if [ "$ydlc" == "0" ];then
        echo "You do not have Latency data for the last 24h. Exiting." ; rm $yd ; exit

else
        echo "You have data for $(date -d @$range24h) - $(date -d @$timenow)"
        rm $yd
fi

echo ''
printf "+ ${bold}Latency Status Reporting${normal}\n"
echo ''
#
printf "+ ${green}Minimum${nc} Latency in the last 24h:\n"
echo ''
echo "$quickestping"
echo ''
#
printf "+ ${red}Maximum${nc} Latency in the last 24h:\n"
echo ''
echo "$longestping"
echo ''


#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`

printf "Latency Status for the last ${green}24h${nc} calculated in: $execution_time\n"

exit
