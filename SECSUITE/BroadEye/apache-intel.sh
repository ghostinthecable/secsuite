#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
apacheoutagescount="$basedir/outagecount.txt"
echoapacheoutagescount="$basedir/outagecountfin.txt"

apacheoutagesinfo="$basedir/outageinfo.txt"
echoapacheoutagesinfo="$basedir/outageinfofin.txt"
#
#SQL Queries;
todaysoutages=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT COUNT(*) AS '' FROM hist_apachestatus WHERE apachestatus = 'Apache Server is Offline!' AND timestamp BETWEEN '$range24h' AND '$timenow';" 2>/dev/null >> $apacheoutagescount ; tr -d "\n" < $apacheoutagescount > $echoapacheoutagescount ; cat $echoapacheoutagescount ; rm $apacheoutagescount $echoapacheoutagescount)
todaysoutagesinfo=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT apachestatus AS '', timestamp AS '' FROM hist_apachestatus WHERE apachestatus LIKE '%Offline%' AND timestamp BETWEEN '$range24h' AND '$timenow';" 2>/dev/null >> $apacheoutagesinfo ; tr -d "\n" < $apacheoutagesinfo > $echoapacheoutagesinfo ; sed -i 's/Apache/ \- Apache/g' $echoapacheoutagesinfo ; cat $echoapacheoutagesinfo ; rm $apacheoutagesinfo $echoapacheoutagesinfo)

#---------------------

if [ "$todaysoutages" == "0" ]
then
        echo "You did not have any Apache outages in the last 24h. Exiting." ; exit
else
        echo "You have data for $(date -d @$range24h) - $(date -d @$timenow)"
fi

echo ''
printf "+ ${bold}Apache Server Status Reporting${normal}\n"
echo ''
#
printf "+ Displaying Apache Server ${red}outages${nc} count reported in the last 24h:\n"
echo "-> Count: $todaysoutages"
#
echo ''
printf "+ Displaying Apache Server ${red}outages${nc} reported in the last 24h:\n"
echo "-> $todaysoutagesinfo"

#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`
printf "Apache Server Status for ${green}$datetoday${nc} calculated in: $execution_time\n"

exit
