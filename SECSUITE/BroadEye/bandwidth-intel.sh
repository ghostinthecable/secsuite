#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
#
#SQL Queries;
#RX Values;
minrxfile="$basedir/minrxfile.txt"
minrxout="$basedir/minrxout.txt"
minrx=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MIN(bytes) AS '' FROM hist_bandwidthstatus WHERE type = 'RX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes ASC LIMIT 1;" 2>/dev/null > $minrxfile ; tr -d "\n" < $minrxfile > $minrxout ; cat $minrxout ; rm $minrxfile $minrxout)
maxrxfile="$basedir/maxrxfile.txt"
maxrxout="$basedir/maxrxout.txt"
maxrx=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MAX(bytes) AS '' FROM hist_bandwidthstatus WHERE type = 'RX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes DESC LIMIT 1;" 2>/dev/null > $maxrxfile ; tr -d "\n" < $maxrxfile > $maxrxout ; cat $maxrxout ; rm $maxrxfile $maxrxout)
#TX Values;
mintxfile="$basedir/mintxfile.txt"
mintxout="$basedir/mintxout.txt"
mintx=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MIN(bytes) AS '' FROM hist_bandwidthstatus WHERE type = 'TX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes ASC LIMIT 1;" 2>/dev/null > $minrxfile ; tr -d "\n" < $minrxfile > $minrxout ; cat $minrxout ; rm $minrxfile $minrxout)
maxtxfile="$basedir/maxtxfile.txt"
maxtxout="$basedir/maxtxout.txt"
maxtx=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MAX(bytes) AS '' FROM hist_bandwidthstatus WHERE type = 'TX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes DESC LIMIT 1;" 2>/dev/null > $maxrxfile ; tr -d "\n" < $maxrxfile > $maxrxout ; cat $maxrxout ; rm $maxrxfile $maxrxout)


#---------------------
#
#Begin Reporting;
#

if [ -s $maxrxout ]
then
        echo "You do not have Bandwidth data in the last 24h. Exiting." ; exit
else
        echo "You have data for $(date -d @$range24h) - $(date -d @$timenow)"
fi

echo ''
printf "+ ${bold}Bandwidth Reporting${normal}\n"
echo ''
#RX Values;
#Min;
printf "+ ${green}Minimum${nc} RX Value:\n"
mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MIN(humanformat) AS LeastAmountReceived, bytes, type FROM hist_bandwidthstatus WHERE type = 'RX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes ASC LIMIT 1;" 2>/dev/null
echo ''
#Max;
printf "+ ${red}Maximum${nc} RX Value:\n"
mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MAX(humanformat) AS LargestAmountReceived, bytes, type FROM hist_bandwidthstatus WHERE type = 'RX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes DESC LIMIT 1;" 2>/dev/null
#
echo ''
#TX Values;
#Min;
printf "+ ${green}Minimum${nc} TX Value:\n"
mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MIN(humanformat) AS LeastAmountSent, bytes, type FROM hist_bandwidthstatus WHERE type = 'TX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes ASC LIMIT 1;" 2>/dev/null
echo ''
#Max;
printf "+ ${red}Maximum${nc} TX Value:\n"
mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status; SELECT MAX(humanformat) AS LargestAmountSent, bytes, type FROM hist_bandwidthstatus WHERE type = 'TX' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY bytes ORDER BY bytes DESC LIMIT 1;" 2>/dev/null
#
printf "\n\n"
printf "* In the last ${green}24h${nc}, your totals are:\n\n"
#
#For le maths;
onemb="1048576"
#
#RX;
rxused=$(echo "$maxrx - $minrx" | bc)
#Get the first 5 chars of output in MB;
rxusedreadable=$(echo "$rxused/$onemb" | bc -l | cut -c1-5)
#Print;
printf "+ Total RX Bytes: $rxused\n"
printf "+ Total RX (in MB): $rxusedreadable\n"
#
echo "- - - - - - - - - - - - - - -"
#TX;
txused=$(echo "$maxtx - $mintx" | bc)
#Get the first 5 chars of output in MB;
txusedreadable=$(echo "$txused/$onemb" | bc -l | cut -c1-5)
#Print;
printf "+ Total TX Bytes: $txused\n"
printf "+ Total TX (in MB): $txusedreadable\n"
#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`

printf "Bandwidth for ${green}$datetoday${nc} calculated in: $execution_time\n"

exit
