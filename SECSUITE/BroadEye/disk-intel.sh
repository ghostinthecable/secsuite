#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
#
diskusagestart="$basedir/startdiskuse.txt"
echodiskusagestart="$basedir/echostartdiskuse.txt"
diskusagestarttemp="$basedir/startdiskusetemp.txt"
echodiskusagestarttemp="$basedir/echostartdiskusetemp.txt"
echodiskusagestartsizeletter="$basedir/echodiskusagestartsizeletter.txt"
#
diskusagefin="$basedir/findiskuse.txt"
echodiskusagefin="$basedir/echofindiskuse.txt"
diskusagefintemp="$basedir/findiskusetemp.txt"
echodiskusagefintemp="$basedir/echofindiskusetemp.txt"
echodiskusagefinsizeletter="$basedir/echodiskusagefinsizeletter.txt"
#
#SQL Queries;
daystart=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT totalused AS '' FROM hist_storagestatus WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY timestamp ASC LIMIT 1;" 2>/dev/null >> $diskusagestart ; tr -d "\n" < $diskusagestart > $echodiskusagestart ; cat $echodiskusagestart ; rm $diskusagestart $echodiskusagestart)
dayfin=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT totalused AS '' FROM hist_storagestatus WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY timestamp DESC LIMIT 1;" 2>/dev/null >> $diskusagefin ; tr -d "\n" < $diskusagefin > $echodiskusagefin ; cat $echodiskusagefin ; rm $diskusagefin $echodiskusagefin)
#
daystarttemp=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT totalused AS '' FROM hist_storagestatus WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY timestamp ASC LIMIT 1;" 2>/dev/null >> $diskusagestarttemp ; tr -d "\n" < $diskusagestarttemp > $echodiskusagestarttemp ; cat $echodiskusagestarttemp | grep -o 'G' >> $echodiskusagestartsizeletter ; sed -i 's/G//g' $echodiskusagestarttemp ; sed -i 's/M//g' $echodiskusagestarttemp ; cat $echodiskusagestarttemp ; rm $diskusagestarttemp $echodiskusagestarttemp)
dayfintemp=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT totalused AS '' FROM hist_storagestatus WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY timestamp DESC LIMIT 1;" 2>/dev/null >> $diskusagefintemp ; tr -d "\n" < $diskusagefintemp > $echodiskusagefintemp ; cat $echodiskusagefintemp | grep -o 'G' >> $echodiskusagefinsizeletter ; sed -i 's/G//g' $echodiskusagefintemp ; sed -i 's/M//g' $echodiskusagefintemp ; cat $echodiskusagefintemp ; rm $diskusagefintemp $echodiskusagefintemp)

#
#---------------------
#
#Begin

if [ -s $daystarttemp ]
then
        echo "You do not have Disk data in the last 24h. Exiting." ; rm $echodiskusagestartsizeletter $echodiskusagefinsizeletter ; exit
else
        echo "You have data for $(date -d @$range24h) - $(date -d @$timenow)"
fi


echo ''
printf "+ ${bold}Disk Usage Reporting:${normal}\n"
echo ''
#
printf "+ Disk Usage Used on $(date -d @$range24h):\n"
echo " $daystarttemp $(cat $echodiskusagestartsizeletter)" ; rm $echodiskusagestartsizeletter
#
echo ''
#
printf "+ Disk Usage Used on $(date -d @$timenow):\n"
echo " $dayfintemp $(cat $echodiskusagefinsizeletter)"
#
echo ''
#
printf "+ Gathering Statistics on your Daily Use in the last 24h:\n"

if (( $(echo "$daystarttemp > $dayfintemp" | bc -l) )); then
#if [ "$daystarttemp" -gt "$dayfintemp" ]; then
    freedspace="$(($daystarttemp-$dayfintemp))"
    printf "* You have ${green}freed up${nc}: $freedspace $(cat $echodiskusagefinsizeletter)\n" ; rm $echodiskusagefinsizeletter
  else
    occupiedspace=$(echo  "$dayfintemp - $daystarttemp" | bc)
#    occupiedspace="$(($dayfintemp-$daystarttemp))"
    printf "* You have ${red}used${nc}: $occupiedspace $(cat $echodiskusagefinsizeletter)\n" ; rm $echodiskusagefinsizeletter
fi

#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`
printf "Disk Usage Statistics in the last ${green}24h${nc} calculated in: $execution_time\n"

exit
