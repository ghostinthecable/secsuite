#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
oneminload="$basedir/oneminload.txt"
echooneminload="$basedir/oneminloadout.txt"
tenminload="$basedir/tenminload.txt"
echotenminload="$basedir/tenminloadout.txt"
fifminload="$basedir/fifminload.txt"
echofifminload="$basedir/fifminloadout.txt"
#
#SQL Queries;
heavyoneminload=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT loadonemin AS '' FROM hist_cpu WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY loadonemin DESC LIMIT 1;" 2>/dev/null >> $oneminload ; tr -d "\n" < $oneminload > $echooneminload ; cat $echooneminload ; rm $oneminload $echooneminload)
heavytenminload=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT loadtenmin AS '' FROM hist_cpu WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY loadtenmin DESC LIMIT 1;" 2>/dev/null >> $tenminload ; tr -d "\n" < $tenminload > $echotenminload ; cat $echotenminload ; rm $tenminload $echotenminload)
heavyfifminload=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT loadfifmin AS '' FROM hist_cpu WHERE timestamp BETWEEN '$range24h' AND '$timenow' ORDER BY loadfifmin DESC LIMIT 1;" 2>/dev/null >> $fifminload ; tr -d "\n" < $fifminload > $echofifminload ; cat $echofifminload ; rm $fifminload $echofifminload)

#
#---------------------

if [ -z "$heavyoneminload" ]
then
      echo "You do not have data in the last 24h. Exiting." ; exit
else
      echo "You have data for $(date -d @$range24h) - $(date -d @$timenow)"
fi

echo ''
printf "+ ${bold}CPU Load Average Information:${normal}\n"
echo ''
printf "+ Displaying ${red}Heaviest${nc} ${bold}One Minute Load${normal} Average for: Last 24h:\n"
echo "-> $heavyoneminload"
echo ''
printf "+ Displaying ${red}Heaviest${nc} ${bold}Ten Minute Load${normal} Average for: Last 24h:\n"
echo "-> $heavytenminload"
echo ''
printf "+ Displaying ${red}Heaviest${nc} ${bold}Fifteen Minute Load${normal} Average for: Last 24h:\n"
echo "-> $heavyfifminload"
#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`
printf "CPU Load Stats for the last ${green}24h${nc} calculated in: $execution_time\n"
