#!/bin/bash
#---------------------
start=$(date +%s.%N)
#Begin Variables;
#MySQL Creds;
mysqluser='username'
mysqlpass='password'
#
#Today's date and time minus 24h range;
timenow=$(date +%s)
onedayinsec="86400"
range24h=$(echo "$timenow - $onedayinsec" | bc)
#ASCII Colours;
bold="\033[1m"
red="\033[91m"
green="\033[92m"
normal="\033[0m"
nc="\033[39m"
#
#Files & Directories;
basedir="/root/scripts/SECSUITE/BroadEye"
minmemories="$basedir/minmem.txt"
echominmemories="$basedir/minmemout.txt"
maxmemories="$basedir/maxmem.txt"
echomaxmemories="$basedir/maxmemout.txt"
#
#SQL Queries;
leastused=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT hostname AS '', min(usedmemory) AS '' FROM hist_memorystatus WHERE type LIKE '%Mem%' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY hostname;" 2>/dev/null >> $minmemories ; tr -d "\n" < $minmemories > $echominmemories ; cat $echominmemories ; rm $minmemories)
maxused=$(mysql --user="$mysqluser" --password="$mysqlpass" -e "USE status;SELECT hostname AS '', max(usedmemory) AS '' FROM hist_memorystatus WHERE type LIKE '%Mem%' AND timestamp BETWEEN '$range24h' AND '$timenow' GROUP BY hostname;" 2>/dev/null >> $maxmemories ; tr -d "\n" < $maxmemories > $echomaxmemories ; cat $echomaxmemories ; rm $maxmemories $echomaxmemories)

#---------------------

FILE=$echominmemories
if [ -f "$FILE" ]; then
        echo "You have data for $datetoday"
        rm $echominmemories
else
        echo "You do not have Memory data for $datetoday. Exiting." ; exit
fi


echo ''
printf "+ ${bold}Memory Status Reporting${normal}\n"
echo ''
#
printf "+ ${green}Minimum${nc} Memory used in the last 24h:\n"
echo ''
echo "$leastused"
echo ''
#
printf "+ ${red}Maximum${nc} Memory used in the last 24h:\n"
echo ''
echo "$maxused"
echo ''


#
echo ''
duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "${green}%.2f${nc} seconds" $duration`
printf "Memory Status for the last ${green}24h${nc} calculated in: $execution_time\n"

exit
