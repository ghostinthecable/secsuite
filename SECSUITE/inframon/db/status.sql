
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apachestatus`
--

DROP TABLE IF EXISTS `apachestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apachestatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) NOT NULL,
  `apachestatus` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apachestatus`
--

LOCK TABLES `apachestatus` WRITE;
/*!40000 ALTER TABLE `apachestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `apachestatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bandwidthstatus`
--

DROP TABLE IF EXISTS `bandwidthstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bandwidthstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `bytes` varchar(255) DEFAULT NULL,
  `humanformat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bandwidthstatus`
--

LOCK TABLES `bandwidthstatus` WRITE;
/*!40000 ALTER TABLE `bandwidthstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `bandwidthstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpu`
--

DROP TABLE IF EXISTS `cpu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `loadonemin` float(5,2) DEFAULT NULL,
  `loadtenmin` float(5,2) DEFAULT NULL,
  `loadfifmin` float(5,2) DEFAULT NULL,
  `x` varchar(10) DEFAULT NULL,
  `y` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpu`
--

LOCK TABLES `cpu` WRITE;
/*!40000 ALTER TABLE `cpu` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_apachestatus`
--

DROP TABLE IF EXISTS `hist_apachestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_apachestatus` (
  `id` int NOT NULL,
  `hostname` varchar(90) NOT NULL,
  `apachestatus` varchar(255) NOT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_apachestatus`
--

LOCK TABLES `hist_apachestatus` WRITE;
/*!40000 ALTER TABLE `hist_apachestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_apachestatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_bandwidthstatus`
--

DROP TABLE IF EXISTS `hist_bandwidthstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_bandwidthstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `bytes` varchar(255) DEFAULT NULL,
  `humanformat` varchar(255) DEFAULT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_bandwidthstatus`
--

LOCK TABLES `hist_bandwidthstatus` WRITE;
/*!40000 ALTER TABLE `hist_bandwidthstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_bandwidthstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_cpu`
--

DROP TABLE IF EXISTS `hist_cpu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_cpu` (
  `id` int NOT NULL,
  `hostname` varchar(90) DEFAULT NULL,
  `loadonemin` float(5,2) DEFAULT NULL,
  `loadtenmin` float(5,2) DEFAULT NULL,
  `loadfifmin` float(5,2) DEFAULT NULL,
  `x` varchar(10) DEFAULT NULL,
  `y` varchar(10) DEFAULT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_cpu`
--

LOCK TABLES `hist_cpu` WRITE;
/*!40000 ALTER TABLE `hist_cpu` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_cpu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_loggedusers`
--

DROP TABLE IF EXISTS `hist_loggedusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_loggedusers` (
  `id` int NOT NULL,
  `username` varchar(90) NOT NULL,
  `pts` varchar(25) NOT NULL,
  `date` varchar(90) NOT NULL,
  `time` varchar(90) NOT NULL,
  `ipaddr` varchar(90) NOT NULL,
  `backuptime` bigint NOT NULL,
  `random` varchar(50) NOT NULL,
  PRIMARY KEY (`random`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_loggedusers`
--

LOCK TABLES `hist_loggedusers` WRITE;
/*!40000 ALTER TABLE `hist_loggedusers` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_loggedusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_memorystatus`
--

DROP TABLE IF EXISTS `hist_memorystatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_memorystatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `totalmemory` varchar(255) DEFAULT NULL,
  `usedmemory` varchar(255) DEFAULT NULL,
  `freememory` varchar(255) DEFAULT NULL,
  `sharedmemory` varchar(255) DEFAULT NULL,
  `cachedmemory` varchar(255) DEFAULT NULL,
  `availablememory` varchar(255) DEFAULT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_memorystatus`
--

LOCK TABLES `hist_memorystatus` WRITE;
/*!40000 ALTER TABLE `hist_memorystatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_memorystatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_srv`
--

DROP TABLE IF EXISTS `hist_srv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_srv` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) NOT NULL,
  `lastping` varchar(255) NOT NULL,
  `importtime` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_srv`
--

LOCK TABLES `hist_srv` WRITE;
/*!40000 ALTER TABLE `hist_srv` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_srv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hist_storagestatus`
--

DROP TABLE IF EXISTS `hist_storagestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hist_storagestatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `filesystem` varchar(255) DEFAULT NULL,
  `totalsize` varchar(255) DEFAULT NULL,
  `totalused` varchar(255) DEFAULT NULL,
  `availablesize` varchar(255) DEFAULT NULL,
  `usedpercentage` varchar(255) DEFAULT NULL,
  `mountlocation` varchar(255) DEFAULT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist_storagestatus`
--

LOCK TABLES `hist_storagestatus` WRITE;
/*!40000 ALTER TABLE `hist_storagestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_storagestatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loggedusers`
--

DROP TABLE IF EXISTS `loggedusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loggedusers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(90) NOT NULL,
  `pts` varchar(25) NOT NULL,
  `date` varchar(90) NOT NULL,
  `time` varchar(90) NOT NULL,
  `ipaddr` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loggedusers`
--

LOCK TABLES `loggedusers` WRITE;
/*!40000 ALTER TABLE `loggedusers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loggedusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memorystatus`
--

DROP TABLE IF EXISTS `memorystatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `memorystatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `totalmemory` varchar(255) DEFAULT NULL,
  `usedmemory` varchar(255) DEFAULT NULL,
  `freememory` varchar(255) DEFAULT NULL,
  `sharedmemory` varchar(255) DEFAULT NULL,
  `cachedmemory` varchar(255) DEFAULT NULL,
  `availablememory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memorystatus`
--

LOCK TABLES `memorystatus` WRITE;
/*!40000 ALTER TABLE `memorystatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `memorystatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `srv`
--

DROP TABLE IF EXISTS `srv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `srv` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) NOT NULL,
  `lastping` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `srv`
--

LOCK TABLES `srv` WRITE;
/*!40000 ALTER TABLE `srv` DISABLE KEYS */;
/*!40000 ALTER TABLE `srv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storagestatus`
--

DROP TABLE IF EXISTS `storagestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storagestatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostname` varchar(90) DEFAULT NULL,
  `filesystem` varchar(255) DEFAULT NULL,
  `totalsize` varchar(255) DEFAULT NULL,
  `totalused` varchar(255) DEFAULT NULL,
  `availablesize` varchar(255) DEFAULT NULL,
  `usedpercentage` varchar(255) DEFAULT NULL,
  `mountlocation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storagestatus`
--

LOCK TABLES `storagestatus` WRITE;
/*!40000 ALTER TABLE `storagestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `storagestatus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-25  7:44:23
