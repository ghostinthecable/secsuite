#!/bin/bash
#ASCII Colours
red='\033[0;31m'
green='\033[0;32m'
nc='\033[0m'
#
basedir="/root/scripts/SECSUITE/inframon/users"
#MySQL Creds (optional)
mysqluser="user"
mysqlpass="pass"
#Files;
dbf="$basedir/dbf.txt"
dbf1="$basedir/dbf1.txt"
dbid="$basedir/dbid.txt"
#
printf "${green} ____  _____ ____ ____  _   _ ___ _____ _____${nc}\n"
printf "${green}/ ___|| ____/ ___/ ___|| | | |_ _|_   _| ____|${nc}\n"
printf "${green}\___ \|  _|| |   \___ \| | | || |  | | |  _|${nc}\n"
printf "${green} ___) | |__| |___ ___) | |_| || |  | | | |___${nc}\n"
printf "${green}|____/|_____\____|____/ \___/|___| |_| |_____|${nc}\n"
printf "${green}           MEMORY-STATUS-INSTALLER${nc}\n"
#
echo ''
while true; do
    read -p "Have you already configured your Database Credentials? (y/n): " yn
    case $yn in
        [Yy]* ) echo "Skipping this step." ; echo '' ; break;;
        [Nn]* ) echo ""
                read -p "Please enter the MySQL User: " mysqluser
                read -s -p "$mysqluser's Password: " mysqlpass ; echo ""
                break;;
        * ) echo "Please answer yes or no.";;
    esac
done
#
echo "Checking Database Configuration..."
mysqlshow --user=$mysqluser --password=$mysqlpass status >> $dbf 2>/dev/null
if grep -q "loggedusers" "$dbf"; then
        printf "${green} Table 'status.loggedusers' exists, continuing...${nc}\n"
fi
if ! grep -q "loggedusers" "$dbf"; then
        printf "${red} Table 'status.loggedusers' Doesn't exist, Installing...${nc}\n"
        mysql --user=$mysqluser --password=$mysqlpass -e "USE status;CREATE TABLE loggedusers (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(90) NOT NULL, pts VARCHAR(20) NOT NULL, date VARCHAR(90) NOT NULL, ipaddr VARCHAR(90) NOT NULL, PRIMARY KEY (id));" 2>/dev/null
        #
        mysqlshow --user=$mysqluser --password=$mysqlpass status >> $dbf1 2>/dev/null
        if grep -q "loggedusers" "$dbf1"; then
        printf "${green}Table 'status.apachestatus' has been created, continuing...${nc}\n"
        fi
        rm $dbf1
fi
rm $dbf
echo "Checking Historical Database Configuration..."
mysqlshow --user=$mysqluser --password=$mysqlpass status >> $dbf 2>/dev/null
if grep -q "hist_loggedusers" "$dbf"; then
        printf "${green} Table 'status.hist_loggedusers' exists, continuing...${nc}\n"
fi
if ! grep -q "hist_loggedusers" "$dbf"; then
        printf "${red} Table 'status.hist_loggedusers' Doesn't exist, Installing...${nc}\n"
        mysql --user=$mysqluser --password=$mysqlpass -e "USE status;CREATE TABLE hist_loggedusers (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(90) NOT NULL, pts VARCHAR(20) NOT NULL, date VARCHAR(90) NOT NULL, ipaddr VARCHAR(90) NOT NULL, backuptime BIGINT NOT NULL, random VARCHAR(50) NOT NULL, PRIMARY KEY (random));" 2>/dev/null
        mysqlshow --user=$mysqluser --password=$mysqlpass status >> $dbf1 2>/dev/null
        if grep -q "hist_loggedusers" "$dbf1"; then
        printf "${green}Table 'status.hist_loggedusers' has been created, continuing...${nc}\n"
        fi
        rm $dbf1
fi
rm $dbf
#

sed -i "s/connectusr/$mysqluser/g" $basedir/users-monitor-local.sh
sed -i "s/secret/$mysqlpass/g" $basedir/users-monitor-local.sh

echo "Executing Users Monitor..."

bash $basedir/users-monitor-local.sh

echo ''
chmod +x $basedir/users-monitor-local.sh
while true; do
    read -p "Do you wish to install this program in your crontab now? (Y/n): " yn
    case $yn in
        [Yy]* ) echo "* * * * * sudo bash $basedir/users-monitor-local.sh" >> /var/spool/cron/crontabs/root; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
exit
