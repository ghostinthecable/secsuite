#!/bin/bash
#
#ASCII Colours
red='\033[0;31m'
green='\033[0;32m'
nc='\033[0m'
#
basedir="/root/scripts/SECSUITE"
#
#
printf "${green} ____  _____ ____ ____  _   _ ___ _____ _____${nc}\n"
printf "${green}/ ___|| ____/ ___/ ___|| | | |_ _|_   _| ____|${nc}\n"
printf "${green}\___ \|  _|| |   \___ \| | | || |  | | |  _|${nc}\n"
printf "${green} ___) | |__| |___ ___) | |_| || |  | | | |___${nc}\n"
printf "${green}|____/|_____\____|____/ \___/|___| |_| |_____|${nc}\n"
printf "${green}            MAIN CONFIGURATION${nc}\n"
#
#Preconfigure directory structure for SECSUITE
#
if [ -d "/root/scripts/" ]; then
        printf " -> /root/scripts/ ${green}✓${nc}\n"
    else
        printf " -> /root/scripts/ ${red}X${nc}\n"
        printf "+ Creating /root/scripts/ (base directory)\n"
        mkdir /root/scripts/
        if [ -d "/root/scripts/" ]; then
                printf " -> /root/scripts/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/" ]; then
        printf " -> $basedir/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/ ${red}X${nc}\n"
        printf "+ Creating $basedir/ (base directory)\n"
        mkdir $basedir/
        if [ -d "$basedir/" ]; then
                printf " -> $basedir/ ${green}✓${nc}\n"
        fi
fi

mv SECSUITE/* $basedir/
mv README.md $basedir/

if [ -d "$basedir/inframon/apachestatus/" ]; then
        printf " -> $basedir/inframon/apachestatus/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/apachestatus/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/apachestatus/ (base directory)\n"
        mkdir $basedir/inframon/apachestatus/
        if [ -d "$basedir/inframon/apachestatus/" ]; then
                printf " -> $basedir/inframon/apachestatus/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/bandwidth/" ]; then
        printf " -> $basedir/inframon/bandwidth/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/bandwidth/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/bandwidth/ (base directory)\n"
        mkdir $basedir/inframon/bandwidth/
        if [ -d "$basedir/inframon/bandwidth/" ]; then
                printf " -> $basedir/inframon/bandwidth/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/cpufiles/" ]; then
        printf " -> $basedir/inframon/cpufiles/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/cpufiles/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/cpufiles/ (base directory)\n"
        mkdir $basedir/inframon/cpufiles/
        if [ -d "$basedir/inframon/cpufiles/" ]; then
                printf " -> $basedir/inframon/cpufiles/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/diskusage/" ]; then
        printf " -> $basedir/inframon/diskusage/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/diskusage/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/diskusage/ (base directory)\n"
        mkdir $basedir/inframon/diskusage/
        if [ -d "$basedir/inframon/diskusage/" ]; then
                printf " -> $basedir/inframon/diskusage/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/memory/" ]; then
        printf " -> $basedir/inframon/memory/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/memory/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/memory/ (base directory)\n"
        mkdir $basedir/inframon/memory/
        if [ -d "$basedir/inframon/memory/" ]; then
                printf " -> $basedir/inframon/memory/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/tempfiles/" ]; then
        printf " -> $basedir/inframon/tempfiles/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/tempfiles/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/tempfiles/ (base directory)\n"
        mkdir $basedir/inframon/tempfiles/
        if [ -d "$basedir/inframon/tempfiles/" ]; then
                printf " -> $basedir/inframon/tempfiles/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/temperaturefiles/" ]; then
        printf " -> $basedir/inframon/temperaturefiles/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/temperaturefiles/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/temperaturefiles/ (base directory)\n"
        mkdir $basedir/inframon/temperaturefiles/
        if [ -d "$basedir/inframon/temperaturefiles/" ]; then
                printf " -> $basedir/inframon/temperaturefiles/ ${green}✓${nc}\n"
        fi
fi

if [ -d "$basedir/inframon/latency-files/" ]; then
        printf " -> $basedir/inframon/latency-files/ ${green}✓${nc}\n"
    else
        printf " -> $basedir/inframon/latency-files/ ${red}X${nc}\n"
        printf "+ Creating $basedir/inframon/latency-files/ (base directory)\n"
        mkdir $basedir/inframon/latency-files/
        if [ -d "$basedir/inframon/latency-files/" ]; then
                printf " -> $basedir/inframon/latency-files/ ${green}✓${nc}\n"
        fi
fi

rm -rf ../secsuite-production/

while true; do
    read -p "Do you wish to configure the database for Inframon now? (Y/n): " yn
    case $yn in
        [Yy]* ) read -p "Please enter the user for MySQL you wish to use: " mysqluser ; read -s -p "$mysqluser's Password: " mysqlpass ; mysql --user="$mysqluser" --password="$mysqlpass" -e "CREATE DATABASE IF NOT EXISTS status" 2>/dev/null ; mysql --user="$mysqluser" --password="$mysqlpass" status < $basedir/inframon/db/status.sql ; echo "Inframon database is now configured." ; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo ''
printf "${green} Your installation of SECSUITE is available at: $basedir/ ${nc}\n"

exit
